#!/bin/sh

export PTEM_HOME=`pwd`
export PTEM_INSTALL_DIR=$PTEM_HOME/output
export PTEM_OPENSOURCE_DIR=$PTEM_HOME/opensource
export PTEM_PLATFORM_DIR=$PTEM_HOME/platform
export PTEM_PRODUCTS_DIR=$PTEM_HOME/products
export PTEM_PROPRIETARY_DIR=$PTEM_HOME/proprietary
