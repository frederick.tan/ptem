/**
 * Filename: Main.c
 * Description:
 * Author: Frederick Tan
 **/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <time.h>

#include <data_def.h>
#include <json_wrapper.h>
#include <microhttpd.h>
#include <transaction_data.h>

/**
 * Invalid method page.
 */
#define METHOD_ERROR "<html><head><title>Illegal request</title></head><body>Go away.</body></html>"

/**
 * Invalid URL page.
 */
#define NOT_FOUND_ERROR "<html><head><title>Not found</title></head><body>Go away.</body></html>"

/**
 * Front page. (/)
 */
#define MAIN_PAGE "<html><head><title>Welcome</title></head><body><form action=\"/2\" method=\"post\">What is your name? <input type=\"text\" name=\"v1\" value=\"%s\" /><input type=\"submit\" value=\"Next\" /></body></html>"

#define COOKIE_NAME "session"
/**
 * Linked list of all active sessions.  Yes, O(n) but a
 * hash table would be overkill for a simple example...
 */
static struct Session *sessions;

void usage(const char *appName)
{
	printf("Usage:\n");
	printf("%s <PORT> <NUMBER_OF_RECORDS>\n", appName);
}

static struct Session *
get_session (struct MHD_Connection *connection)
{ 
  struct Session *ret;
  const char *cookie;
  
  cookie = MHD_lookup_connection_value (connection,
                                        MHD_COOKIE_KIND,
                                        COOKIE_NAME);
  if (cookie != NULL)
    {
      /* find existing session */
      ret = sessions;
      while (NULL != ret)
        {
          if (0 == strcmp (cookie, ret->sid))
            break;
          ret = ret->next;
        }
      if (NULL != ret)
        {
          ret->rc++;
          return ret;
        }
    }
  /* create fresh session */
  ret = calloc (1, sizeof (struct Session));
  if (NULL == ret)
    {
      fprintf (stderr, "calloc error: %s\n", strerror (errno));
      return NULL;
    }
  /* not a super-secure way to generate a random session ID,
     but should do for a simple example... */
  snprintf (ret->sid,
            sizeof (ret->sid),
            "%X%X%X%X",
            (unsigned int) rand (),
            (unsigned int) rand (),
            (unsigned int) rand (),
            (unsigned int) rand ());
  ret->rc++; 
  ret->start = time (NULL);
  ret->next = sessions;
  sessions = ret;
  return ret;
}

/**
 * Type of handler that generates a reply.
 *
 * @param cls content for the page (handler-specific)
 * @param mime mime type to use
 * @param session session information
 * @param connection connection to process
 * @param MHD_YES on success, MHD_NO on failure
 */
typedef int (*PageHandler)(const void *cls,
                           const char *mime,
                           struct Session *session,
                           struct MHD_Connection *connection,
					  	   const char *upload_data,
					  	   size_t *upload_data_size
						  );


/**
 * Entry we generate for each page served.
 */
struct Page
{
  /**
   * Acceptable URL for this page.
   */
  const char *url;

  /**
   * Mime type to set for the page.
   */
  const char *mime;

  /**
   * Handler to call to generate response.
   */
  PageHandler handler;

  /**
   * Extra argument to handler.
   */
  const void *handler_cls;
};

/**
 * Add header to response to set a session cookie.
 *
 * @param session session to use
 * @param response response to modify
 */
static void
add_session_cookie (struct Session *session,
                    struct MHD_Response *response)
{
  char cstr[256];
  snprintf (cstr,
            sizeof (cstr),
            "%s=%s",
            COOKIE_NAME,
            session->sid);
  if (MHD_NO ==
      MHD_add_response_header (response,
                               MHD_HTTP_HEADER_SET_COOKIE,
                               cstr))
    {
      fprintf (stderr,
               "Failed to set session cookie header!\n");
    }
}

static void
request_completed_callback (void *cls,
                            struct MHD_Connection *connection,
                            void **con_cls,
                            enum MHD_RequestTerminationCode toe)
{
	struct Request *request = *con_cls;
	(void)cls;         /* Unused. Silent compiler warning. */
	(void)connection;  /* Unused. Silent compiler warning. */
	(void)toe;         /* Unused. Silent compiler warning. */

	if (NULL == request)
		return;
	if (NULL != request->session)
		request->session->rc--;
	if (NULL != request->pp)
		MHD_destroy_post_processor (request->pp);
	free (request);
}

static int
display_terminals (const void *cls,
              const char *mime,
              struct Session *session,
              struct MHD_Connection *connection,
			  const char *upload_data,
			  size_t *upload_data_size)
{
	printf("display_terminal\n");
	//printf("%s\n", trManager.displayTerminals());
	int ret;
	size_t slen;
	char *reply;
	struct MHD_Response *response;
	(void)cls; /* Unused. Silent compiler warning. */
	slen = strlen (MAIN_PAGE) + strlen (session->value_1);
	reply = malloc (slen + 1);
	if (NULL == reply)
		return MHD_NO;

	//unsigned int terminalId = trManager.createTerminal();
	//jsonWrapper.parse(jdtTransaction, upload_data, *upload_data_size);
	char displayTerminals[1024];
	trManager.displayTerminals(displayTerminals);
	printf("%s\n", displayTerminals);
  
	snprintf (reply,
            slen + 1,
            MAIN_PAGE,
            session->value_1);
  /* return static form */
  response = MHD_create_response_from_buffer (slen,
                                              (void *) reply,
                                              MHD_RESPMEM_MUST_FREE);
  if (NULL == response)
  {
    free (reply);
    return MHD_NO;
  }
  add_session_cookie (session, response);
  MHD_add_response_header (response,
                           MHD_HTTP_HEADER_CONTENT_ENCODING,
                           mime);
  ret = MHD_queue_response (connection,
                            MHD_HTTP_OK,
                            response);
  MHD_destroy_response (response);
  return ret;
}

static int
add_transaction (const void *cls,
              const char *mime,
              struct Session *session,
              struct MHD_Connection *connection,
			  const char *upload_data,
			  size_t *upload_data_size)
{
	printf("add_terminal\n");
	int ret;
	size_t slen;
	char *reply;
	struct MHD_Response *response;
	(void)cls; /* Unused. Silent compiler warning. */
	slen = strlen (MAIN_PAGE) + strlen (session->value_1);
	reply = malloc (slen + 1);
	if (NULL == reply)
		return MHD_NO;

	//unsigned int terminalId = trManager.createTerminal();
	jsonWrapper.parse(jdtTransaction, upload_data, *upload_data_size);
  
	snprintf (reply,
            slen + 1,
            MAIN_PAGE,
            session->value_1);
  /* return static form */
  response = MHD_create_response_from_buffer (slen,
                                              (void *) reply,
                                              MHD_RESPMEM_MUST_FREE);
  if (NULL == response)
  {
    free (reply);
    return MHD_NO;
  }
  add_session_cookie (session, response);
  MHD_add_response_header (response,
                           MHD_HTTP_HEADER_CONTENT_ENCODING,
                           mime);
  ret = MHD_queue_response (connection,
                            MHD_HTTP_OK,
                            response);
  MHD_destroy_response (response);
  return ret;
}

static int
add_terminal (const void *cls,
              const char *mime,
              struct Session *session,
              struct MHD_Connection *connection,
			  const char *upload_data,
			  size_t *upload_data_size)
{
	printf("add_terminal\n");
	int ret;
	size_t slen;
	char *reply;
	struct MHD_Response *response;
	(void)cls; /* Unused. Silent compiler warning. */
	slen = strlen (MAIN_PAGE) + strlen (session->value_1);
	reply = malloc (slen + 1);
	if (NULL == reply)
		return MHD_NO;

	unsigned int terminalId = trManager.createTerminal();
  
	snprintf (reply,
            slen + 1,
            MAIN_PAGE,
            session->value_1);
  /* return static form */
  response = MHD_create_response_from_buffer (slen,
                                              (void *) reply,
                                              MHD_RESPMEM_MUST_FREE);
  if (NULL == response)
  {
    free (reply);
    return MHD_NO;
  }
  add_session_cookie (session, response);
  MHD_add_response_header (response,
                           MHD_HTTP_HEADER_CONTENT_ENCODING,
                           mime);
  ret = MHD_queue_response (connection,
                            MHD_HTTP_OK,
                            response);
  MHD_destroy_response (response);
  return ret;
}

/**
 * Handler used to generate a 404 reply.
 *
 * @param cls a 'const char *' with the HTML webpage to return
 * @param mime mime type to use
 * @param session session handle
 * @param connection connection to use
 */
static int
not_found_page (const void *cls,
                const char *mime,
                struct Session *session,
                struct MHD_Connection *connection,
				const char *upload_data,
				size_t *upload_data_size)
{
  int ret;
  struct MHD_Response *response;
  (void)cls;     /* Unused. Silent compiler warning. */
  (void)session; /* Unused. Silent compiler warning. */

  /* unsupported HTTP method */
  response = MHD_create_response_from_buffer (strlen (NOT_FOUND_ERROR),
                                              (void *) NOT_FOUND_ERROR,
                                              MHD_RESPMEM_PERSISTENT);
  if (NULL == response)
    return MHD_NO;
  ret = MHD_queue_response (connection,
                            MHD_HTTP_NOT_FOUND,
                            response);
  MHD_add_response_header (response,
                           MHD_HTTP_HEADER_CONTENT_ENCODING,
                           mime);
  MHD_destroy_response (response);
  return ret;
}


/**
 * List of all pages served by this HTTP server.
 */
static struct Page postPages[] =
{
	{ "/Terminals/add", "text/html", &add_terminal, NULL },
	{ "/Transactions/add", "text/html", &add_transaction, NULL },
	{ NULL, NULL, &not_found_page, NULL } /* 404 */
};

static struct Page getPages[] =
{
	{ "/Terminals", "text/html", &display_terminals, NULL}
};
/**
 * Clean up handles of sessions that have been idle for
 * too long.
 */
static void
expire_sessions ()
{
  struct Session *pos;
  struct Session *prev;
  struct Session *next;
  time_t now;

  now = time (NULL);
  prev = NULL;
  pos = sessions;
  while (NULL != pos)
    {
      next = pos->next;
      if (now - pos->start > 60 * 60)
        {
          /* expire sessions after 1h */
          if (NULL == prev)
            sessions = pos->next;
          else
            prev->next = next;
          free (pos);
        }
      else
        prev = pos;
      pos = next;
    }
}

static int
process_data(void *cls,
               enum MHD_ValueKind kind,
               const char *key,
               const char *filename,
               const char *content_type,
               const char *transfer_encoding,
               const char *data, uint64_t off, size_t size)
{
	printf("Inside post_iterator\n");
	struct Request *request = cls;
	struct Session *session = request->session;
	(void)kind;              /* Unused. Silent compiler warning. */
	(void)filename;          /* Unused. Silent compiler warning. */
	(void)content_type;      /* Unused. Silent compiler warning. */
	(void)transfer_encoding; /* Unused. Silent compiler warning. */

	printf("PI key=%s data=%s\n", key, data);
  if (0 == strcmp ("DONE", key))
    {
      fprintf (stdout,
               "Session `%s' submitted `%s', `%s'\n",
               session->sid,
               session->value_1,
               session->value_2);
      return MHD_YES;
    }
  if (0 == strcmp ("v1", key))
    {
      if (size + off >= sizeof(session->value_1))
        size = sizeof (session->value_1) - off - 1;
      memcpy (&session->value_1[off],
              data,
              size);
      session->value_1[size+off] = '\0';
      return MHD_YES;
    }
  if (0 == strcmp ("v2", key))
    {
      if (size + off >= sizeof(session->value_2))
        size = sizeof (session->value_2) - off - 1;
      memcpy (&session->value_2[off],
              data,
              size);
      session->value_2[size+off] = '\0';
      return MHD_YES;
    }
  fprintf (stderr,
           "Unsupported form value `%s'\n",
           key);
  return MHD_YES;
}

int connectionHandler(void *cls,
					  struct MHD_Connection *connection,
					  const char *url,
					  const char *method,
					  const char *version,
					  const char *upload_data,
					  size_t *upload_data_size,
					  void **ptr)
{
	struct MHD_Response *response;
	struct Request *request;
	struct Session *session;
	int ret;
	unsigned int i;
	(void)cls;               /* Unused. Silent compiler warning. */
	(void)version;           /* Unused. Silent compiler warning. */

	request = *ptr;

	if (NULL == request)
	{
		request = calloc(1, sizeof (struct Request));
		if (NULL == request)
		{
			fprintf (stderr, "calloc error: %s\n", strerror (errno));
			printf("calloc error: %s\n", strerror (errno));
			return MHD_NO;
		}
		*ptr = request;
		if (0 == strcmp (method, MHD_HTTP_METHOD_POST))
		{
			/* This does not support JSON content, so we need to hack it */
			request->pp = MHD_create_post_processor(connection, 64 * 1024,
                                                   &process_data, request);
			printf("Created post processor...%llu\n", (unsigned long long)request->pp);
			return MHD_YES;
		}
		return MHD_YES;
	}
	if (NULL == request->session)
	{
		request->session = get_session(connection);
		if (NULL == request->session)
		{
			fprintf (stderr, "Failed to setup session for `%s'\n", url);
			printf ("Failed to setup session for `%s'\n", url);
			return MHD_NO; /* internal error */
		}
	}
	session = request->session;
  
	session->start = time (NULL);
  
	if (0 == strcmp (method, MHD_HTTP_METHOD_POST))
	{
		if (0 != *upload_data_size)
		{
			/* evaluate POST data */
			i=0;
			while ((postPages[i].url != NULL) &&
			       (0 != strcmp (postPages[i].url, url)))
				i++;
			ret = postPages[i].handler (postPages[i].handler_cls,
                              postPages[i].mime,
                              session, connection, upload_data, upload_data_size);
			*upload_data_size = 0;
			return ret;
		}
		/* done with POST data, serve response */
		MHD_destroy_post_processor (request->pp);
		request->pp = NULL;
		method = MHD_HTTP_METHOD_GET; /* fake 'GET' */
		if (NULL != request->post_url)
			url = request->post_url;
	}
	if (0 == strcmp (method, MHD_HTTP_METHOD_GET))
	{
		i=0;
		while ((getPages[i].url != NULL) &&
		       (0 != strcmp (getPages[i].url, url)))
			i++;
		ret = getPages[i].handler (getPages[i].handler_cls,
                             getPages[i].mime,
                             session, connection, NULL, NULL);
		if (ret != MHD_YES)
        fprintf (stderr, "Failed to create page for `%s'\n",
                 url);
      return ret;

	}

	/* unsupported HTTP method */
	response = MHD_create_response_from_buffer (strlen (METHOD_ERROR),
                                              (void *) METHOD_ERROR,
                                              MHD_RESPMEM_PERSISTENT);
	ret = MHD_queue_response (connection,
                            MHD_HTTP_NOT_ACCEPTABLE,
                            response);
	MHD_destroy_response (response);

	return MHD_YES;
}

int main(int argc, char *argv[])
{
	unsigned short port = 0;
	unsigned int numberOfTerminals = 0;
	unsigned int flag = MHD_USE_THREAD_PER_CONNECTION | MHD_USE_AUTO | MHD_USE_INTERNAL_POLLING_THREAD;
	struct MHD_Daemon *d = NULL;
	struct timeval tv;
	struct timeval *tvp;
	fd_set rs;
	fd_set ws;
	fd_set es;
	MHD_socket max;
	MHD_UNSIGNED_LONG_LONG mhd_timeout;

	if (argc != 3)
	{
		usage(argv[0]);
		return 1;
	}
	else
	{
		port = atoi(argv[1]);
		numberOfTerminals = atoi(argv[2]);
	}
	trManager.init(numberOfTerminals);
	srand ((unsigned int) time (NULL));
	d = MHD_start_daemon(flag,
						port,
						NULL, NULL,
						&connectionHandler, NULL,
						MHD_OPTION_CONNECTION_TIMEOUT, (unsigned int) 60,
						MHD_OPTION_NOTIFY_COMPLETED, &request_completed_callback, NULL,
						MHD_OPTION_END);

	if (d == NULL)
		return 1;

	char ch = 0;
	do
	{
		ch = getchar();
	} while (ch != 'x');


	MHD_stop_daemon (d);

	return 0;
}
