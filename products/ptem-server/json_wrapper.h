/**
 * Filename: json_wrapper.h
 * Brief:
 * Author: Frederick Tan
 */

#ifndef _JSON_WRAPPER_H_
#define _JSON_WRAPPER_H_

#include <json-parser/json.h>
#include <stdbool.h>

typedef enum
{
	jdtTransaction = 0,
	jdtUnknown = 0xff
} JSONDataType;

typedef struct
{
	json_value *value;
	bool (*parse)(JSONDataType type, const char* str, int length);
} JSONWrapper;

extern JSONWrapper jsonWrapper;

#endif
