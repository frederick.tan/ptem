/**
 * Filename: data_defs.h
 *
 **/

#ifndef _DATA_DEFS_H_
#define _DATA_DEFS_H_

/**
 * State we keep for each user/session/browser.
 */
struct Session
{
	/**
	 * We keep all sessions in a linked list.
	 */
	struct Session *next;

	/**
	 * Unique ID for this session.
	 */
	char sid[33];

	/**
	 * Reference counter giving the number of connections
	 * currently using this session.
	 */
	unsigned int rc;

	/**
	 * Time when this session was last active.
	 */
	time_t start;

	/**
	 * String submitted via form.
	 */
	char value_1[64];

	/**
	 * Another value submitted via form.
	 */
	char value_2[64];
};

/**
 * Data kept per request.
 */
struct Request
{
	/**
	 * Associated session.
	 */
	struct Session *session;

	/**
	 * Post processor handling form data (IF this is
	 * a POST request).
	 */
	struct MHD_PostProcessor *pp;

	/**
	 * URL to serve in response to this POST (if this request
	 * was a 'POST')
	 */
	const char *post_url;
};

#endif
