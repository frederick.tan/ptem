/**
 * Filename: json_wrapper.c
 * Brief:
 * Author: Frederick Tan
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <json_wrapper.h>
#include <transaction_data.h>

static bool parse(JSONDataType type, const char* str, int length);

JSONWrapper jsonWrapper =
{
	.value = NULL,
	.parse = parse
};

static bool process_transaction_data(json_value* value)
{
	int length = 0, x = 0;
	bool cardTypeValid = false, txTypeValid = false;
	char cardType[20];
	char txType[20];
	unsigned long long terminalId = 0;

	if (value == NULL)
	{
		printf("values is null \n");
		return false;
	}

	length = value->u.object.length;

	if (length != 3)
	{
		printf("wrong length %d \n", value->u.object.length);
		return false;
	}

	memset(cardType, 0, sizeof(cardType));
	memset(txType, 0, sizeof(txType));
	for (x = 0; x < length; x++)
	{
		json_value *valueNext = value->u.object.values[x].value;
		if (strcmp(value->u.object.values[x].name, "cardType") == 0)
		{
			if (valueNext->type != json_string)
			{
				printf("cardType type error %d\n", valueNext->type);
				return false;
			}
			strcpy(cardType, valueNext->u.string.ptr);
			cardTypeValid = true;
		}
		else if (strcmp(value->u.object.values[x].name, "TransactionType") == 0)
		{
			if (valueNext->type != json_string)
			{
				printf("txType type error %d\n", valueNext->type);
				return false;
			}
			strcpy(txType, valueNext->u.string.ptr);
			txTypeValid = true;
		}
		else if (strcmp(value->u.object.values[x].name, "TerminalId") == 0)
		{
			if (valueNext->type != json_integer)
			{
				printf("TerminalId type error %d\n", valueNext->type);
				return false;
			}
			char dummy[20];
			sprintf(dummy, "%" PRId64 "\n", value->u.integer);
			printf("%" PRId64 "\n", value->u.integer);
			terminalId = atoi(dummy);
		}
	}

	if (cardTypeValid && txTypeValid && terminalId)
	{
		printf("Added Transaction for terminal %llu: card Type: %s, transaction Type: %s\n", terminalId, cardType, txType);
		trManager.addTransaction(terminalId, cardType, txType);
	}
	return true;
}

bool parse(JSONDataType type, const char* str, int length)
{
	bool status = false;
	printf("JSON Wrapper parse [%d] %s\n", type, str);
	json_value* value = json_parse((json_char*)str, length);
	if (type == jdtTransaction)
	{
		status = process_transaction_data(value);
	}
	json_value_free(value);
	return status;
}
