/**
 * Filename: transaction_data.h
 * Brief:
 * Author: Frederick Tan
 **/
#ifndef _TRANSACTION_DATA_H_
#define _TRANSACTION_DATA_H_

#include <stdbool.h>

typedef enum
{
	ctVisa			= 0,
	ctMasterCard	= 1,
	ctEFTOS			= 2,
	ctUnknown		= 3
} CardType;

typedef enum
{
	ttCheque		= 0,
	ttSavings		= 1,
	ttCredit		= 2,
	ttUnknown		= 3
} TransactionType;

typedef struct Transaction
{
	CardType cardType;
	TransactionType transactionType;
	struct Transaction *next;
	struct Transaction *prev;
} Transaction_t;

typedef struct
{
	int terminalId;
	int transactionCount;
	Transaction_t *head;
} Terminal;

typedef struct
{
	bool status;
	bool (*init)(int numberOfRecords);
	unsigned int (*createTerminal)();
	bool (*removeTerminal)(int terminalId);
	bool (*addTransaction)(int terminalId, const char *cardType, const char *transactionType);
	bool (*removeTransaction)(int transactionId);
	void (*displayTerminals)(char *data);
} TerminalRecordManager;

extern TerminalRecordManager trManager;
#endif
