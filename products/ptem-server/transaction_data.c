/**
 * Filename: transaction_data.h
 * Brief:
 * Author: Frederick Tan
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <transaction_data.h>

static Terminal *terminalList = NULL;
static int terminalCount = 0;
static int TerminalMaxCount = 0;
static bool init(int numberOfRecords);
static unsigned int createTerminal();
static bool removeTerminal(int terminalId);
static bool addTransaction(int terminalId, const char *cardType, const char *transactionType);
static bool removeTransaction(int transactionId);
static void displayTerminals(char *displayBuffer);

TerminalRecordManager trManager =
{
	.status = false,
	.init = init,
	.createTerminal = createTerminal,
	.removeTerminal = removeTerminal,
	.addTransaction = addTransaction,
	.removeTransaction = removeTransaction,
	.displayTerminals = displayTerminals
};

typedef struct
{
	const char *text;
	CardType type;
} CardTypeMapping;

typedef struct
{
	const char *text;
	TransactionType type;
} TxTypeMapping;

CardTypeMapping ctMap[] =
{
	{"Visa", ctVisa}, {"MasterCard", ctMasterCard}, {"EFTOS", ctEFTOS}
};

TxTypeMapping txTypeMap[] =
{
	{"Cheque", ttCheque}, {"Savings", ttSavings}, {"Credit", ttCredit}
};

bool init(int numberOfRecords)
{
	terminalList = (Terminal*)calloc(numberOfRecords, sizeof(Terminal));
	terminalCount = 0;

	if (terminalList != NULL)
	{
		trManager.status = true;
		TerminalMaxCount = numberOfRecords;
	}

	return trManager.status;
}

Terminal* findTerminal(int terminalId)
{
	Terminal *terminal = terminalList;
	Terminal *targetTerminal = 0;

	for (int index = 0; index < terminalCount; index++, terminal++)
	{
		if (terminal->terminalId == terminalId)
		{
			targetTerminal = terminal;
			break;
		}
	}

	return targetTerminal;
}

void displayTerminals(char *displayBuffer)
{
	printf("Inside displayTerminals(%d)>>>\n", terminalCount);
	char *buffer = displayBuffer;
	memset(displayBuffer, 0, sizeof(displayBuffer));
	buffer += sprintf(buffer, "{\"terminals\" : [");
	for (int index = 0; index < terminalCount; index++)
    {
		if (terminalList[index].terminalId == 0)
		{
			printf("terminalList is NULL [%d]\n", index);
			break;
		}
		buffer += sprintf(buffer, "{\"terminalId\" : %d", terminalList[index].terminalId);
		buffer += sprintf(buffer, "\"Transactions\" : [");
		Transaction_t *temp = terminalList[index].head;
		while (temp)
		{
			buffer += sprintf(buffer, "{\"cardType\":\"%s\",\"TransactionType\":\"%s\"}", ctMap[temp->cardType].text, txTypeMap[temp->transactionType].text);
			printf("%d: %s\n", temp->cardType, ctMap[temp->cardType].text);
			printf("%d: %s\n", temp->transactionType, txTypeMap[temp->transactionType].text);
			if (temp->next)
			{
				buffer += sprintf(buffer, ",");
			}
			temp = temp->next;
		}
		buffer += sprintf(buffer, "]");
    }
	buffer += sprintf(buffer, "]}");
	printf("<<<Inside displayTerminals()\n");
}

unsigned int createTerminal()
{
	
	if ((terminalList == NULL) ||
		(terminalCount >= TerminalMaxCount))
	{
		return false;
	}

	unsigned int terminalId = 0;
	do
	{
		terminalId = rand();
	} while (findTerminal(terminalId));

	terminalList[terminalCount].terminalId = terminalId;
	terminalList[terminalCount].transactionCount = 0;
	terminalList[terminalCount].head = NULL;

	terminalCount++;
	printf("Created terminal with ID: %d\n", terminalId);
	return terminalId;
}

bool removeTerminal(int terminalId)
{
	bool found = false;
	Terminal *terminal = terminalList;
	for (int index = 0; index < TerminalMaxCount; index++, terminal++)
	{
		if (terminal->terminalId == terminalId)
		{
			found = true;
			// TODO: Delete all memory allocation and rearrange terminalList
			//for ()
			//{
			//}
		}
	}

	return false;
}

bool addTransaction(int terminalId, const char *cardType, const char *transactionType)
{
	bool found = false;
	Terminal *terminal = NULL;
	CardType cType = ctUnknown;
	TransactionType tType = ttUnknown;

	for (int i = 0; i < sizeof(ctMap) / sizeof(CardTypeMapping); i++)
	{
		if (strcmp(cardType, ctMap[i].text) == 0)
		{
			cType = ctMap[i].type;
		}
	}

	for (int i = 0; i < sizeof(txTypeMap) / sizeof(TxTypeMapping); i++)
	{
		if (strcmp(transactionType, txTypeMap[i].text) == 0)
		{
			tType = txTypeMap[i].type;
		}
	}

	for (int index = 0; index < TerminalMaxCount; index++)
	{
		terminal = &terminalList[index];
		printf("%d %d %d\n", index, terminal->terminalId, (int)terminalId);
		if (terminal->terminalId == (int)terminalId)
		{
			printf("Found terminal for adding\n");
			found = true;
			Transaction_t *transaction = (Transaction_t*)calloc(1, sizeof(Transaction_t));
			Transaction_t *temp = terminal->head;
			transaction->cardType = cType;
			transaction->transactionType = tType;
			transaction->next = NULL;
			transaction->prev = NULL;
			if (terminal->head == NULL)
			{
				terminal->head = transaction;
				break;
			}
			while(temp->next != NULL) temp = temp->next;
			temp->next = transaction;
			transaction->prev = temp;
			break;
		}
	}
	
	return found;
}

bool removeTransaction(int transactionId)
{
	return false;
}
